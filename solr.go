package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func (c *Config) schema() (doc []byte, err error) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", c.schemaURL(), nil)
	resp, err := client.Do(req)
	defer resp.Body.Close()
	doc, err = ioutil.ReadAll(resp.Body)
	return doc, err
}

func (c *Config) ingest(rec Record) (err error) {
	c.ValidateRecord(rec)
	recs := []Record{rec}
	data, err := json.Marshal(recs)
	fmt.Println(string(data))
	res, err := http.Post(c.updateURL(), "application/json", bytes.NewBuffer(data))
	defer res.Body.Close()
	txt, err := ioutil.ReadAll(res.Body)
	fmt.Println(string(txt))
	return err
}

func (c *Config) remove(rec Record) (err error) {
	id := rec["id"].(string)
	data := fmt.Sprintf(`{"delete": {"id": %s}}`, id)
	fmt.Println(data)
	res, err := http.Post(c.updateURL(), "application/json", bytes.NewBuffer([]byte(data)))
	txt, err := ioutil.ReadAll(res.Body)
	fmt.Println(string(txt))
	res.Body.Close()
	return err
}

func (c *Config) ValidateRecord(rec Record) {
	for key := range rec {
		_, ok := c.Fields[key]
		if !ok {
			fmt.Printf("Invalid key detected in record, removing it. (%s)\n", key)
			delete(rec, key)
		}
	}
	for key := range c.Fields {
		if c.Fields[key].Static != nil {
			rec[key] = c.Fields[key].Static
		}
	}
}
