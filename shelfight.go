package main

import (
	"encoding/json"
	"fmt"
    "flag"
	"net/http"
)

// Return the configured schema.  Used by the Shared Shelf publisher
// to allow field mapping.
func (c *Config) getSchema(w http.ResponseWriter, r *http.Request) {
	schema, err := json.Marshal(c)
	if err != nil {
		fmt.Println(w, "FAILED: %s", err)
		fmt.Println(err)
		return
	}

	fmt.Fprint(w, string(schema))
}

// Handle Shared Shelf publishing calls.
func (c *Config) handleUpdate(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var data Record
	err := decoder.Decode(&data)
	if err != nil {
		fmt.Fprintln(w, "JSON could not be decoded", err)
		fmt.Println(err)
		return
	}
	switch r.Method {
	case "POST":
		doUpdate(c, w, data)
	case "DELETE":
		doDelete(c, w, data)
	case "PUT":
		doFail(w, http.StatusMethodNotAllowed)
	case "HEAD":
		doFail(w, http.StatusMethodNotAllowed)
	case "GET":
		doFail(w, http.StatusMethodNotAllowed)
	case "PATCH":
		doFail(w, http.StatusMethodNotAllowed)
	}
}

func doFail(w http.ResponseWriter, statusCode int) {
	w.WriteHeader(statusCode)
	w.Write([]byte("Call failed"))
}

// Ingest data into the configured SOLR index
func doUpdate(c *Config, w http.ResponseWriter, data Record) {
	err := c.ingest(data)
	if err != nil {
		fmt.Println(err)
		doFail(w, http.StatusBadRequest)
		return
	}
	fmt.Fprintf(w, "Ingestion successful")
}

// Delete a record from the configured SOLR index
func doDelete(c *Config, w http.ResponseWriter, data Record) {
	err := c.remove(data)
	if err != nil {
		doFail(w, http.StatusBadRequest)
		return
	}
	fmt.Fprintf(w, "Deletion successful")
}

// Start up a web server and listen for Shared Shelf publishing requests
func main() {
    var port = flag.String("port", "7777", "Port to listen on")
    flag.Parse()
	c := loadConfig(flag.Arg(0))
	http.HandleFunc("/schema", c.getSchema)
	http.HandleFunc("/update", c.handleUpdate)
	fmt.Printf("Shelfight starting up, listening on port: %s\n", *port)
	http.ListenAndServe(fmt.Sprintf(":%s", *port), nil)
}
